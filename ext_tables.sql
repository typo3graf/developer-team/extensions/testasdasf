#
# Table structure for table 'tx_testify_domain_model_testimonials'
#
CREATE TABLE tx_testify_domain_model_testimonials (

	name varchar(255) DEFAULT '' NOT NULL,
	position varchar(255) DEFAULT '' NOT NULL,
	company_name varchar(255) DEFAULT '' NOT NULL,
	company_url varchar(255) DEFAULT '' NOT NULL,
	rating int(11) DEFAULT '0' NOT NULL,
	picture int(11) unsigned NOT NULL default '0',
	description text,

);

#
# Table structure for table 'tx_testify_domain_model_testimonials'
#
CREATE TABLE tx_testify_domain_model_testimonials (
	categories int(11) unsigned DEFAULT '0' NOT NULL,
);
