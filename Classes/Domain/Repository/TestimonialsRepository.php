<?php
namespace Typo3graf\Testify\Domain\Repository;

/***
 *
 * This file is part of the "Testimonials" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Mike Tölle <mtoelle@typo3graf-media.de>, Typo3graf media-agentur
 *
 ***/
/**
 * The repository for Testimonials
 */
class TestimonialsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = ['sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING];
}
