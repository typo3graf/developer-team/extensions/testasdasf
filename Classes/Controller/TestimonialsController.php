<?php
namespace Typo3graf\Testify\Controller;

/***
 *
 * This file is part of the "Testimonials" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Mike Tölle <mtoelle@typo3graf-media.de>, Typo3graf media-agentur
 *
 ***/
/**
 * TestimonialsController
 */
class TestimonialsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * testimonialsRepository
     *
     * @var \Typo3graf\Testify\Domain\Repository\TestimonialsRepository
     * @inject
     */
    protected $testimonialsRepository = null;

    /**
     * action list
     */
    public function listAction()
    {
        $testimonials = $this->testimonialsRepository->findAll();
        $this->view->assign('testimonials', $testimonials);
    }
}
