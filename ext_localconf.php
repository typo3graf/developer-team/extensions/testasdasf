<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function () {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Typo3graf.Testify',
            'Testimonials',
            [
                'Testimonials' => 'list'
            ],
            // non-cacheable actions
            [
                'Testimonials' => ''
            ]
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    testimonials {
                        iconIdentifier = testify-plugin-testimonials
                        title = LLL:EXT:testify/Resources/Private/Language/locallang_db.xlf:tx_testify_testimonials.name
                        description = LLL:EXT:testify/Resources/Private/Language/locallang_db.xlf:tx_testify_testimonials.description
                        tt_content_defValues {
                            CType = list
                            list_type = testify_testimonials
                        }
                    }
                }
                show = *
            }
       }'
        );
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

        $iconRegistry->registerIcon(
            'testify-plugin-testimonials',
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:testify/Resources/Public/Icons/user_plugin_testimonials.svg']
        );
    }
);
