<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function () {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Typo3graf.Testify',
            'Testimonials',
            'Testimonials'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('testify', 'Configuration/TypoScript', 'Testimonials');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_testify_domain_model_testimonials', 'EXT:testify/Resources/Private/Language/locallang_csh_tx_testify_domain_model_testimonials.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_testify_domain_model_testimonials');
    }
);
