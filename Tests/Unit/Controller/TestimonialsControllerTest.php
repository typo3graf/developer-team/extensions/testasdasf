<?php
namespace Typo3graf\Testify\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Mike Tölle <mtoelle@typo3graf-media.de>
 */
class TestimonialsControllerTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Typo3graf\Testify\Controller\TestimonialsController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Typo3graf\Testify\Controller\TestimonialsController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllTestimonialssFromRepositoryAndAssignsThemToView()
    {

        $allTestimonials = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $testimonialsRepository = $this->getMockBuilder(\Typo3graf\Testify\Domain\Repository\TestimonialsRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $testimonialsRepository->expects(self::once())->method('findAll')->will(self::returnValue($allTestimonials));
        $this->inject($this->subject, 'testimonialsRepository', $testimonialsRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('testimonials', $allTestimonials);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}
