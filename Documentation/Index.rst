﻿.. ---------------------------------------------------------------
   This is the start file. It gets displayed as first page
   https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/GeneralConventions/DirectoryFilenames.html#supported-filenames-and-formats
   ---------------------------------------------------------------

.. ---------------------------------------------------------------
   More information about creating an extension manual:
   https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingDocForExtension/CreateWithExtensionBuilder.html
   ---------------------------------------------------------------

.. ---------------------------------------------------------------
   comments start with 2 dots and a blank
   they can continue on the next line
   ---------------------------------------------------------------

.. ---------------------------------------------------------------
   every .rst file should include Includes.txt
   use correct path!
   ---------------------------------------------------------------

.. include:: Includes.txt

.. ---------------------------------------------------------------
   Every manual should have a start label for cross-referencing to
   start page. Do not remove this!
   ---------------------------------------------------------------

.. _start:

.. ---------------------------------------------------------------
   This is the doctitle
   ---------------------------------------------------------------

=============================================================
Testimonials
=============================================================

:Extensionkey:
    testify
:Version:
    |release|
:Language:
    en
:Description:
    Testimonials are easy to use the plugin that allows users to add Testimonials to the sidebar, as a widget, or to embed testimonials into a Page or Post using the shortcode. Testimonials also allow you to insert a list of all Testimonials or output a Random Testimonial. The testimonial is 100% responsive for all devices.
:Keywords:
     gdfgdfg, fsdgsdfgsd, reterter, hjghjghj
:Copyright:
    2020
:Author:
    Mike Tölle
:Email:
    mtoelle@t3graf.de
:License:
   This extension documentation is published under the `CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`__ (Creative Commons) license

The content of this document is related to TYPO3 CMS,
a GNU/GPL CMS/Framework available from `typo3.org
<https://typo3.org/>`_ .


**Table of Contents:**

.. toctree::
   :maxdepth: 3
   :titlesonly:

   Introduction/Index
   User/Index
   Installation/Index
   Configuration/Index
   Developer/Index
   KnownProblems/Index
   ToDoList/Index
   ChangeLog/Index
   Sitemap
